cmake_minimum_required(VERSION 3.5)
project(averageAlongAxis)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")

ADD_DEFINITIONS(
        -DWM_DP
        -Dlinux64
        -DNoRepository
)

set ( PROJECT_LINK_LIBS libfiniteVolume.so libmeshTools.so libsampling.so libaverageAlongAxisFunctionObject.so
        libgenericPatchFields.so libOpenFOAM.so)

include_directories( $ENV{FOAM_SRC}/OpenFOAM/lnInclude
                     $ENV{FOAM_SRC}/OSspecific/POSIX/lnInclude
                     $ENV{FOAM_SRC}/finiteVolume/lnInclude
                     $ENV{FOAM_SRC}/meshTools/lnInclude
                     $ENV{FOAM_SRC}/smapling/lnInclude
                     lnInclude
                     .. )


link_directories($ENV{FOAM_LIBBIN} $ENV{FOAM_USER_LIBBIN} )

add_executable( execAverageAlongAxis execAverageAlongAxis.C )

target_compile_options( execAverageAlongAxis PRIVATE -fPIC -O3 -Wall -Wextra -Wno-unused-parameter -Wold-style-cast -Wnon-virtual-dtor)

target_link_libraries( execAverageAlongAxis ${PROJECT_LINK_LIBS} )