cmake_minimum_required(VERSION 3.5)
project(averageAlongAxis)

add_subdirectory (execAverageAlongAxis)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")


ADD_DEFINITIONS(
        -DWM_DP
        -Dlinux64
        -DNoRepository
)

set ( PROJECT_LINK_LIBS libfiniteVolume.so libmeshTools.so )

include_directories( $ENV{FOAM_SRC}/OpenFOAM/lnInclude
                     $ENV{FOAM_SRC}/OSspecific/POSIX/lnInclude
                     $ENV{FOAM_SRC}/finiteVolume/lnInclude
                     $ENV{FOAM_SRC}/meshTools/lnInclude
                     lnInclude )


link_directories($ENV{FOAM_LIBBIN})

add_library( libaverageAlongAxisFunctionObject SHARED averageAlongAxis.C averageAlongAxisFunctionObject.C )

target_link_libraries( libaverageAlongAxisFunctionObject ${PROJECT_LINK_LIBS} )

