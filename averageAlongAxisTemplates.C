/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "averageAlongAxis.H"
#include "OFstream.H"
#include "meshTools.H"
#include "Time.H"
#include "SortableList.H"

//#include "vtkMesh.H"
//#include "readFields.H"
//#include "writeFuns.H"

//#include "internalWriter.H"
//#include "patchWriter.H"

//#include "writePointSet.H"
//#include "surfaceMeshWriter.H"
//#include "writeSurfFields.H"




// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

template<class T>
Foam::Field<T> Foam::averageAlongAxis::regionSum(const Field<T>& cellField) const
{
    // create a field of the same type as T, initialized to 0
    // and of size equal to the amount of regions the mesh is 
    // split into.
    Field<T> regionField(cellRegion_().nRegions(), pTraits<T>::zero);


    forAll(cellRegion_(), cellI)
    {
        regionField[cellRegion_()[cellI]] += cellField[cellI];
    }

    // Global sum
    Pstream::listCombineGather(regionField, plusEqOp<T>());
    Pstream::listCombineScatter(regionField);

    return regionField;
}

template<class T>
Foam::Field<T> Foam::averageAlongAxis::regionAverage(const Field<T>& cellField) const
{

    scalar cellsPerRegion = regionCount_[0];
    scalar nRegions = cellRegion_().nRegions();


    Field<T> averagedField(cellsPerRegion, pTraits<T>::zero);

    SortableList<label> sortedCellRegion(cellRegion_());
    labelList sortMap = sortedCellRegion.indices();


    //perform the avraging
    forAll(averagedField, cellI)
    {
        for (int i = 0; i < nRegions; i++)
        {
            averagedField[cellI] += cellField[sortMap[cellI+i*cellsPerRegion]];
        }
    }


    //reduce(averagedField, sumOp<T>());


    averagedField /= nRegions;

    //output for debugging
    if (false)
    {
        OFstream str("testAverage");
        str << "cellRegion_()" << endl;
        str << cellRegion_() <<  endl;
        str << "sortMap" << endl;
        str << sortMap << endl;
        str << "sortedCellRegion" << endl;
        str << sortedCellRegion << endl;
        str << "averagedField" << endl;
        str << averagedField;
    }

    return averagedField;
}

template<class T>
Foam::Field<T> Foam::averageAlongAxis::patchAverage(const fvPatchField<T>& patchField) const
{

    //scalar cellsPerRegion = regionCount_[0];
    scalar nRegions = cellRegion_().nRegions();

    label facesPerRegion = 0;

    labelList faceRegion(patchField.size());

    forAll(patchField.patch(), faceI)
    {
        label faceCellI = patchField.patch().faceCells()[faceI];

        faceRegion[faceI] = cellRegion_()[faceCellI];

        // Check the amount of faces in layer 0, the rest should have the same amount
        if(cellRegion_()[faceCellI] == 0)
        {
            facesPerRegion++;
        }

    }

    //Info << "facePerRegion " << facesPerRegion << endl;

    Field<T> averagedPatchField(patchField.size()/nRegions, pTraits<T>::zero);

    SortableList<label> sortedCellRegion(faceRegion);
    labelList sortMap = sortedCellRegion.indices();


    //perform the avraging
    forAll(averagedPatchField, faceI)
    {
        for (int i = 0; i < nRegions; i++)
        {
            averagedPatchField[faceI] += patchField[sortMap[faceI+i*facesPerRegion]];
        } 
    }

    averagedPatchField /= nRegions;
    
    return averagedPatchField;
}

/*template<class T>
void Foam::averageAlongAxis::fieldToVTK
(
    const Field<T>& cellField, 
    word name,
    OFstream & csv
) const
{
    Foam::fvMesh mesh
    (
        Foam::IOobject
        (
            Foam::fvMesh::defaultRegion,
            obr_.time().timeName(),
            obr_,
            Foam::IOobject::MUST_READ
        )
    );
    vtkMesh vMesh(mesh);

    labelList patchIDs;

    const polyBoundaryMesh& patches = mesh_.boundaryMesh();

    forAll(averagePatchNames_,i)
    {
        const label patchI = patches.findPatchID(averagePatchNames_[i]);
        patchIDs.append(patchI);

    }

    IOobject dummyIO
    (
        "dummy",
        obr_.time().timeName(),
        mesh_,
        IOobject::READ_IF_PRESENT,
        IOobject::NO_WRITE
    );

    volScalarField field
    (
        dummyIO,
        mesh_,
        dimensionedScalar("field", dimensionSet(0,0,0,0,0,0,0), 0.0)
    );

    field.boundaryField()[patchIDs[0]] = cellField;
    field.boundaryField()[patchIDs[1]] = cellField;


    patchWriter writer
    (
        vMesh,
        0,
        0,
        "testVtk.vtk",
        patchIDs 
    );

    volScalarField & ptrField = field; 

    PtrList<volScalarField> ptrList;
    ptrList.append(ptrField);

    writer.write(ptrList);
    

}*/

template<class T>
void Foam::averageAlongAxis::fieldToCSV
(
    const Field<T>& cellField, 
    word name,
    OFstream & csv  
) const
{
    pointField cC = cellCentres_;


    csv << "index x y z " << name << endl;
    forAll(cC, i)
    {
        csv << i << " " << cC[i].component(vector::X) << " " << cC[i].component(vector::Y) << " " << cC[i].component(vector::Z) << " " << cellField[i] << endl;
    }
}

template<class T>
void Foam::averageAlongAxis::patchFieldToCSV
(
    const Field<T> & patchField, 
    word name,
    const vectorField & aFC,
    label startIndex,
    OFstream & csv
) const
{

    forAll(aFC, i)
    {
        csv << i+startIndex << " " << aFC[i].component(vector::X) << " " << aFC[i].component(vector::Y) << " " << aFC[i].component(vector::Z) << " " << patchField[i] << endl;
    }
}

template<class T>
Foam::Field<T> Foam::averageAlongAxis::collapse
(
    const Field<T>& cellField
) const
{
    Field<T> regionField = regionAverage(cellField);
    return regionField;
}


// ************************************************************************* //
