/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

Class
    Foam::averageAlongAxis

Description
    Does averaging of fields over layers of cells. Assumes layered mesh.

SourceFiles
    averageAlongAxis.C

\*---------------------------------------------------------------------------*/

#ifndef averageAlongAxis_H
#define averageAlongAxis_H

#include "fvCFD.H"
#include "regionSplit.H"
#include "direction.H"
#include "scalarField.H"
#include "OFstream.H"
#include "polyMesh.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{


/*---------------------------------------------------------------------------*\
                           Class averageAlongAxis Declaration
\*---------------------------------------------------------------------------*/

class averageAlongAxis
{

    // Private data

        static const NamedEnum<vector::components, 3> vectorComponentsNames_;

        //- Name
        word name_;
        
        //- Object registry the class is registered in
        const objectRegistry & obr_;

        bool active_;

        //- Per cell the global region
        autoPtr<regionSplit> cellRegion_;

        //- Per global region the number of cells (scalarField so we can use
        //  field algebra)
        scalarField regionCount_;

        //- From sorted region back to unsorted global region
        labelList sortMap_;

        //- Cell centers averaged across regions
        pointField cellCentres_;

        //- patches to seed from
        wordList seedPatchNames_;

        //- patches to average on 
        wordList averagePatchNames_;

        //- faces of the patches to seed from;
        labelList startFaces_;

        //- fields to average;
        wordList fields_;

        //- Path to where the output will be stored
        fileName path_;

        //- the mesh
        const fvMesh & mesh_;

    // Private Member Functions

        void walkOppositeFaces
        (
            boolList& blockedFace
        );

        void calcLayeredRegions();

        //- Disallow default bitwise copy construct and assignment
        averageAlongAxis(const averageAlongAxis&);
        void operator=(const averageAlongAxis&);


public:
       // Run-time type information 
        TypeName("averageAlongAxis");

    // Constructors

        averageAlongAxis(const word &, const objectRegistry &, const dictionary&, bool loadFromFiles = false);

    // Destructor
        virtual ~averageAlongAxis(){}
        

    // Member Functions

        //- Return name of the set of field averages
        virtual const word& name() const
        {
            return name_;
        }    

        //- Sum field per region
        template<class T>
        Field<T> regionSum(const Field<T>& cellField) const;

        //- Average field across regions
        template<class T>
        Field<T> regionAverage(const Field<T>& cellField) const;
        
        //- Average pach field across regions
        template<class T>
        Field<T> patchAverage(const fvPatchField<T>& cellField) const;

        //Write averaged internal field as CSV along with cell centra coords
        template<class T>
        void fieldToCSV
        (
            const Field<T>& cellField,
            word fieldName,
            OFstream & csv
        ) const;

        // Write average field as vtk
        /*template<class T>
        void fieldToVTK
        (
            const Field<T>& cellField,
            word fieldName,
            OFstream & csv
        ) const;*/

        //Write averaged patch field as CSV along with face centra coords
        template<class T>
        void patchFieldToCSV
        (
            const Field<T>& patchField,
            word fieldName,
            const vectorField & Cf, 
            label startIndex,
            OFstream & csv
        ) const;

        //- collapse a field 
        template<class T>
        Field<T> collapse
        (
            const Field<T>& vsf
        ) const;

        virtual void start();

        virtual void execute()
        {}

        virtual void end()
        {}

        virtual void read(const dictionary &);

        virtual void write();

        virtual void timeSet()
        {}

        virtual void movePoints(const polyMesh &)
        {}

        virtual void updateMesh(const mapPolyMesh &)
        {}

};


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#ifdef NoRepository
#   include "averageAlongAxisTemplates.C"
#endif

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

#endif

// ************************************************************************* //
